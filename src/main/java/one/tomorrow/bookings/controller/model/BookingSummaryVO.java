package one.tomorrow.bookings.controller.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class BookingSummaryVO {
    private final UUID id;
    private final Long amount;
    private final CurrencyVO currency;
    private final UnitVO unit;
    private final ZonedDateTime bookingDate;
    private final ZonedDateTime valueDate;
    private final String beneficiary;
    private final String booking;

    public enum CurrencyVO {
        EUR
    }

    public enum UnitVO {
        cents
    }
}
