package one.tomorrow.bookings.controller;

import lombok.RequiredArgsConstructor;
import one.tomorrow.bookings.controller.model.BookingSummaryVO;
import one.tomorrow.bookings.service.BookingsService;
import one.tomorrow.bookings.service.model.Amount;
import one.tomorrow.bookings.service.model.BookingSummary;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/bookings")
public class BookingsController {

    private final BookingsService service;

    @GetMapping
    public List<BookingSummaryVO> getBookingSummaries() {
        return service.getBookingSummaries().stream()
                .map(this::createBookingSummaryVO)
                .collect(Collectors.toList());
    }

    private BookingSummaryVO createBookingSummaryVO(BookingSummary bookingSummary) {
        Long amountValue;
        BookingSummaryVO.CurrencyVO currency;
        BookingSummaryVO.UnitVO unit;

        Amount amount = bookingSummary.getAmount();
        switch (amount.getCurrency()) {
            case EUR:
                amountValue = amount.getValue().movePointRight(2).longValue();
                currency = BookingSummaryVO.CurrencyVO.EUR;
                unit = BookingSummaryVO.UnitVO.cents;
                break;
            default:
                throw new IllegalArgumentException("Unsupported currency: " + amount.getCurrency().name());
        }

        return new BookingSummaryVO(
                bookingSummary.getId(),
                amountValue,
                currency,
                unit,
                bookingSummary.getBookingDate(),
                bookingSummary.getValueDate(),
                bookingSummary.getBeneficiary(),
                bookingSummary.getBooking());
    }

}

