package one.tomorrow.bookings.util;

import one.tomorrow.bookings.repository.BookingsRepository;
import one.tomorrow.bookings.repository.model.BookingEntity;
import one.tomorrow.bookings.service.model.User;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Component
public class BookingsBootstrap {

    public BookingsBootstrap(BookingsRepository repository) {
        User user = SecurityHelper.getCurrentAutheticatedUser();
        UUID userId = user.getId();
        UUID accountId = UUID.randomUUID();

        UUID otherBookingId = UUID.fromString("7d496244-6ae3-49db-b073-2dd3b62df677");
        UUID otherUserId = UUID.randomUUID();
        UUID otherAccountId = UUID.randomUUID();

        List<BookingEntity> list = List.of(
                new BookingEntity(
                        UUID.randomUUID(),
                        userId,
                        accountId,
                        BigDecimal.valueOf(-99.98),
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        "Awesome Online Shop",
                        "Online Order NO 123456789",
                        "More detailed information about the online order",
                        "DE07123412341234123412",
                        "SOKBDEBBXXX"),
                new BookingEntity(
                        UUID.randomUUID(),
                        userId,
                        accountId,
                        BigDecimal.valueOf(-11.23),
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        "Your Local Supermarket",
                        "Organic groceries for all",
                        "More detailed information about the purchase",
                        "DE07123412341234123412",
                        "SOKBDEBBXXX"),
                new BookingEntity(
                        UUID.randomUUID(),
                        userId,
                        accountId,
                        BigDecimal.valueOf(25.00),
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        "Your Friends at tommorow",
                        "A gift",
                        "Party. Yeah!",
                        "DE07123412341234123412",
                        "SOKBDEBBXXX"),
                new BookingEntity(
                        otherBookingId,
                        otherUserId,
                        otherAccountId,
                        BigDecimal.valueOf(100000.00),
                        ZonedDateTime.now(),
                        ZonedDateTime.now(),
                        "You should not see this",
                        "This is private",
                        String.format("Seriously. This is private and not accesable for %s %s (%s)", user.getFirstName(), user.getName(), user.getId()),
                        "DE07123412341234123412",
                        "SOKBDEBBXXX")
        );
        repository.saveAll(list);
    }


}
