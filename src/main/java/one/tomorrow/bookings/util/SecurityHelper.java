package one.tomorrow.bookings.util;

import one.tomorrow.bookings.service.model.User;

import java.util.UUID;

public final class SecurityHelper {

    private SecurityHelper() {
    }

    private static User user = new User(
            UUID.randomUUID(),
            "carol.danvers@tomorrow.one",
            "Danvers",
            "Carol");

    public static User getCurrentAutheticatedUser() {
        return user;
    }

}
