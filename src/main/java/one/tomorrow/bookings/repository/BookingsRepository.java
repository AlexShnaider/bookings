package one.tomorrow.bookings.repository;

import one.tomorrow.bookings.repository.model.BookingEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface BookingsRepository extends CrudRepository<BookingEntity, UUID> {
    List<BookingEntity> findByUserId(UUID userId);
}
